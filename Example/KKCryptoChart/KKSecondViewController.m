//
//  KKSecondViewController.m
//  KKCryptoChart_Example
//
//  Created by 杨鹏 on 2022/6/28.
//  Copyright © 2022 HenryDang. All rights reserved.
//

#import "KKSecondViewController.h"
#import <KKCryptoChart/KKCryptoChart.h>

@interface KKSecondViewController ()

@end

@implementation KKSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Second Chart";
    self.view.backgroundColor = [UIColor blackColor];
    [self initChartUI];
}

- (void)initChartUI {
    KKCryptoChartView *cryptoChartView = [[KKCryptoChartView alloc] initWithFrame:CGRectMake(14, 104, self.view.frame.size.width - 28, 500)];
    [self.view addSubview:cryptoChartView];
    KKCryptoChartGlobalConfig *config = [[KKCryptoChartGlobalConfig alloc] initWithLocale:@"zh" timeType:@"15" coinPrecision:@"8" tradeVolumePrecision:@"5"  environment:@"prod" coinCode:@"SHIB_USDT"];//MATIC_USDT,FTM_USDT,DOSE_USDT
    cryptoChartView.config = config;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
