//
//  KKCryptoChartGlobalConfig.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/4.
//

#import <Foundation/Foundation.h>

// 国际化语言
typedef NS_ENUM(NSInteger, KKCryptoChartLocaleType) {
    KKCryptoChartLocaleTypeSimplifiedChinese = 1, // 简体中文
    KKCryptoChartLocaleTypeTraditionalChinese,  // 繁体中文
    KKCryptoChartLocaleTypeEnglish // 英文
};

// 项目环境
typedef NS_ENUM(NSInteger, KKCryptoChartEnvironmentType) {
    KKCryptoChartEnvironmentTypeDev = 1, // dev环境
    KKCryptoChartEnvironmentTypeBeta, // beta环境 aweb v3
    KKCryptoChartEnvironmentTypeProduct,  // 生产环境
    KKCryptoChartEnvironmentTypeBetaOld, // beta环境 老环境 兼容接口 方便调试，稳定后可以移除
};

NS_ASSUME_NONNULL_BEGIN

@interface KKCryptoChartGlobalConfig : NSObject

// 国际化语言
@property (nonatomic, assign) KKCryptoChartLocaleType locale;
// 时间段
@property (nonatomic, assign) NSString *timeType;
// 币精度
@property (nonatomic, strong) NSString *coinPrecision;
// 成交量精度
@property (nonatomic, strong) NSString *tradeVolumePrecision;
// 环境
@property (nonatomic, assign) KKCryptoChartEnvironmentType environment;
// 盘口名称
@property (nonatomic, strong) NSString *coinCode;


/// 全局配置初始化接口
/// - Parameters:
///   - locale: 当前语言  默认 en
///
///      "hk" => 香港，
///
///      "zh" => 大陆
///
///      "en" => 英语
///   - timeType:
///   - coinPrecision:
///   - tradeVolumePrecision:
///   - environment: 环境配置  默认 prod
///
///      beta/beta_green/beta_red => beta
///
///      prod/prod_green/prod_red => prod
///
///      dev => dev
///   - coinCode:
- (instancetype)initWithLocale:(NSString *)locale timeType:(NSString *)timeType coinPrecision:(NSString *)coinPrecision tradeVolumePrecision:(NSString *)tradeVolumePrecision environment:(NSString *)environment coinCode:(NSString *)coinCode;

@end

NS_ASSUME_NONNULL_END
