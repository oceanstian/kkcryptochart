//
//  KKStatusStorageUtil.m
//  KKCryptoChart
//
//  Created by Henry on 2021/11/8.
//

#import "KKCryptoChartStatusStorageUtil.h"
#import "KKCryptoChart-Swift.h"

@implementation KKCryptoChartStatusStorageUtil

+ (void)removeAllTabbarStatus {
    [[TabbarSelectUtil share] removeAllTabbarStatus];
}

@end
