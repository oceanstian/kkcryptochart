//
//  EncryptionUtil.m
//  KKCryptoChart
//
//  Created by Henry on 2022/2/24.
//

#import "EncryptionUtil.h"
#import <CommonCrypto/CommonDigest.h>

@implementation EncryptionUtil

+ (NSString* )md5:(NSString* )input {
    if (!input) {
        return nil;
    }
    const char * pointer = [input UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];

    CC_MD5(pointer, (CC_LONG)strlen(pointer), md5Buffer);

    NSMutableString *string = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [string appendFormat:@"%02x",md5Buffer[i]];
    }
    return string;
}

+ (NSString *)base64:(NSString *)input {
    if (!input) {
        return nil;
    }
    NSData *sData = [input dataUsingEncoding:NSUTF8StringEncoding];
    NSData *base64Data = [sData base64EncodedDataWithOptions:0];
    NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
    return baseString;
}

@end
