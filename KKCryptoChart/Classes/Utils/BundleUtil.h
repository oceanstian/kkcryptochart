//
//  BundleUtil.h
//  KKCryptoChart
//
//  Created by apple on 2021/3/16.
//

#import <Foundation/Foundation.h>

@interface BundleUtil : NSObject

+(NSBundle *)getBundle;

+(NSString *)getFilePathFromBundle:(NSString *)filePathInBundle;

@end

