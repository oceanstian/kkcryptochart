//
//  KKCryptoMoreSelector.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/10.
//

#define MORE_SELECTOR_COUNT 6

#import <UIKit/UIKit.h>
@class TabbarSelectUtil;

NS_ASSUME_NONNULL_BEGIN

@protocol KKCryptoMoreSelectorDelegate <NSObject>

@optional

// main chart selector changed
- (void)didMoreSelectorSelected:(NSInteger)type title:(NSString *)title;

@end
// MARK: - at the top of chartview show 15m\1h\4h... or 1m\5m\30m\1w\1M\1Y
@interface KKCryptoMoreSelector : UIView

@property (assign, nonatomic) id<KKCryptoMoreSelectorDelegate> delegate;

- (void)setUpTypes:(NSArray<NSNumber *> *)types titles:(NSArray<NSString *> *)titles;
- (void)setAllBtnUnselected;

@end

NS_ASSUME_NONNULL_END
