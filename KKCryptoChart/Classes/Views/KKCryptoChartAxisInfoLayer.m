//
//  KKCryptoChartAxisInfoLayer.m
//  KKCryptoChart
//
//  Created by Henry on 2022/3/7.
//

#import "KKCryptoChartAxisInfoLayer.h"
#define AXIS_INFO_LAYER_BACKGROUND_COLOR [UIColor colorWithRed:174/255.f green:179/255.f blue:191/255.f alpha:1.0f]

@interface KKCryptoChartAxisInfoLayer()

@property (nonatomic, strong) CATextLayer *textLayer;

@end

@implementation KKCryptoChartAxisInfoLayer

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = AXIS_INFO_LAYER_BACKGROUND_COLOR.CGColor;
        self.xOffset = 0;
        self.yOffset = 0;
        [self initTextLayer];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.cornerRadius = frame.size.height / 2;
    self.textLayer.frame = CGRectMake(self.xOffset, self.yOffset, frame.size.width - self.xOffset * 2, frame.size.height - self.yOffset * 2);
}

- (void)initTextLayer {
    _textLayer = [[CATextLayer alloc] init];
    _textLayer.foregroundColor = UIColor.whiteColor.CGColor;
    _textLayer.contentsScale = [UIScreen mainScreen].scale;
    [self addSublayer:_textLayer];
}

- (void)setText:(NSString *)text {
    _textLayer.string = text;
}

- (void)setFontSize:(NSInteger)fontSize {
    _textLayer.fontSize = fontSize;
}

- (void)setAlignmentMode:(CATextLayerAlignmentMode)alignmentMode {
    _textLayer.alignmentMode = alignmentMode;
}

@end
