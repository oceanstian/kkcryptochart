//
//  KKWebSocket.h
//  KKCryptoChart
//
//  Created by Henry on 2022/2/23.
//

#import <Foundation/Foundation.h>

@protocol KKWebSocketDelegate <NSObject>

@optional

- (void)didReceiveKlineData:(NSDictionary *)klineData;

@end

@interface KKWebSocket : NSObject

@property (nonatomic, assign) id<KKWebSocketDelegate> delegate;
@property (nonatomic, strong) NSString *symbol;
@property (nonatomic, strong) NSString *fromSymbol;
@property (nonatomic, strong) NSString *resolution;
@property (nonatomic, assign) BOOL isOpen;

- (instancetype)initWithURLString:(NSString *)urlString symbol:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution;
- (void)open;
- (void)close;
- (void)updateSocket:(NSString *)symbol fromSymbol:(NSString *)fromSymbol resolution:(NSString *)resolution;

@end

