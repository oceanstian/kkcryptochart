//
//  KKKLineBaseApi.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import "KKKLineBaseApi.h"

NSString *const BaseBetaURLOld = @"https://api.beta.test-aspendigital.co/market/kline/init";

// aweb v3 接口
NSString *const BaseDevURL = @"https://api2.dev.test-aspendigital.co/v3/market/kline/init";

NSString *const BaseBetaURL = @"https://api2.beta.test-aspendigital.co/v3/market/kline/init";

NSString *const BaseProdURL = @"https://api2.aspendigital.co/v3/market/kline/init";

@interface KKKLineBaseApi()

@property (nonatomic, strong) NSString *baseUrl;

@end

@implementation KKKLineBaseApi

- (void)setEnvironment:(KKCryptoChartEnvironmentType)environment {
    _environment = environment;
    switch (environment) {
        case KKCryptoChartEnvironmentTypeDev:
            _baseUrl = BaseDevURL;
            break;
        case KKCryptoChartEnvironmentTypeBeta:
            _baseUrl = BaseBetaURL;
            break;
        case KKCryptoChartEnvironmentTypeProduct:
            _baseUrl = BaseProdURL;
            break;
        case KKCryptoChartEnvironmentTypeBetaOld:
            _baseUrl = BaseBetaURLOld;
        default:
            _baseUrl = BaseProdURL;
            break;
    }
}

- (NSString *)getBaseURL {
    return self.baseUrl;
}

- (NSString *)getCustomURL {
    return nil;
}

- (NSString *)getCompletedURL {
    return nil;
}

@end
